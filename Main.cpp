#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {

	// Holds image
	Mat image;

	// Read the file
	image = imread("../test.jpg", IMREAD_COLOR);

	// Check if data is invalid
	if (!image.data) {
		cout << "ERROR: Could not open or find the image!" << endl;
		return -1;
	}

	// Show our image
	imshow("Our image", image);

	// Wait for a keystroke to close the window
	waitKey(-1);

	// Cleanup all windows
	destroyAllWindows();

	return 0;
}

