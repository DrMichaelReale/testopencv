# TestOpenCV

This is a minimalist OpenCV example that can be built with CMake.

If you're using Windows, donuts to dollars you have downloaded the binary installation, which at least as of 3.1, only has support for x64.

So, in CMake:  
1. Set your compiler to Visual Studio 14 2015 Win64  
2. Configure  
3. When CMake complains it cannot find OpenCV, set OpenCV_DIR to <wherever OpenCV is installed>/build  
4. Configure again and then Generate  

